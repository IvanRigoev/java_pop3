import telnetlib
import time

test_port = 110
delay = 0.00000001

tn_list = []

num_usr = 1000

for i in range(num_usr):
    tn_list.append(telnetlib.Telnet("127.0.0.1", port=test_port))

    mail = "USER ivan45654@mail.ru\r\n"
    tn_list[i].write(bytes(mail, encoding='utf-8'))

    time.sleep(delay)

for i in range(num_usr):
    mail = "PASS qwerty\r\n"
    tn_list[i].write(bytes(mail, encoding='utf-8'))

    time.sleep(delay)

for i in range(num_usr):
    mail = "STAT\r\n"
    tn_list[i].write(bytes(mail, encoding='utf-8'))

    time.sleep(delay)

for i in range(num_usr):
    mail = "QUIT\r\n"
    tn_list[i].write(bytes(mail, encoding='utf-8'))

    time.sleep(delay)