package pop3server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.io.File;

public class ClientThread extends Thread {
    Socket clientSocket;
    PrintWriter out = null;
    ArrayList<Integer> list_to_del = new ArrayList<>();

    static String CAPA = "CAPA"; // список дополнительных команд
    static String USER = "USER";
    static String PASS = "PASS";
    static String STAT = "STAT";
    static String LIST = "LIST";
    static String UIDL = "UIDL"; // без него не грузит в the bat (уникальный индентификатор письма)
    static String RETR = "RETR";
    static String QUIT = "QUIT";
    static String NOOP = "NOOP";
    static String DELE = "DELE";
    static String TOP = "TOP";

    static String CRLF = "\r\n";
    static String EOM = ".\r\n";

    String password = "";
    String user_inbox = "";
    String path_to_mailbox = "database" + File.separator + "inbox";

    boolean authorization = true;
    boolean transaction = false;
    boolean update = false;

    ClientThread(Socket clientsocket) {
        this.clientSocket = clientsocket;
    }

    private void CAPA_available_cmds() {
        out.println("+OK CAPA list follows" + CRLF);
        out.println(USER + CRLF);
        out.println(TOP + CRLF);
        out.println(STAT + CRLF);
        out.println(LIST + CRLF);
        out.println(QUIT + CRLF);
        out.println(DELE + CRLF);
        out.println(EOM);
    }

    private void USER_parse_login(String client_inp) {
        StringTokenizer st = new StringTokenizer(client_inp, " ");
        st.nextToken();

        try {
            user_inbox = st.nextToken();

            try {
                BufferedReader buff = new BufferedReader(new FileReader("database" + File.separator + "login.txt"));

                while (true) {
                    try {
                        String line = buff.readLine();

                        if (line == null) {
                            out.println("-ERR never heard of mailbox name");
                            break;  // я его случайно удалил что ли? был же.
                        } else {
                            StringTokenizer st1 = new StringTokenizer(line, "USERPA: \n");
                            while (st1.hasMoreTokens()) {
                                if (user_inbox.equals(st1.nextToken())) {
                                    password = st1.nextToken();
                                    out.println("+OK name is a valid mailbox");
                                    return;
                                } else st1.nextToken();
                            }
                        }

                    } catch (IOException ex) {
                        System.out.println("Can't read the login file");
                        System.exit(-2);
                    }

                }

            } catch (FileNotFoundException ex) {
                System.out.println("Can't find the login file");
                System.exit(-1);
            }


        } catch (Exception e) {
            out.println("-ERR never heard of mailbox name");
        }

    }


    private void PASS_password_control(String client_inp) {
        StringTokenizer st = new StringTokenizer(client_inp, " ");
        st.nextToken();
        String client_pass = st.nextToken();
        if (client_pass.equals(password)) {
            out.println("+OK maildrop locked and ready");
            authorization = false;
            transaction = true;
            path_to_mailbox = path_to_mailbox + File.separator + user_inbox + File.separator;
        } else out.println("-ERR invalid password");
    }

    private void STAT_discovery_dir() {
        File path = new File(path_to_mailbox + File.separator);
        File[] list = path.listFiles();

        int size = 0;
        for (File file : list) {
            size += file.length();
        }
        System.out.println("+OK " + list.length + " " + size);
        out.println("+OK " + list.length + " " + size);
    }

    private void LIST_size_list() {

        File path = new File(path_to_mailbox + File.separator);

        File[] list = path.listFiles();

        // количество байт всех сообщений
        int size = 0;
        for (File file : list) {
            size += file.length();
        }
        out.println("+OK " + list.length + " messages (" + size + " octets)" + CRLF);
        System.out.println("+OK " + list.length + " messages (" + size + " octets)" + CRLF);
        for (int i = 1; i <= list.length; i++) {
            out.println(i + " " + list[i - 1].length());
            System.out.println(i + " " + list[i - 1].length());
        }
        System.out.println(EOM);
        out.println(EOM);
    }

    //    по идее у каждого сообщения должен быть уникальный номер типа хэш, но так тоже есть уникальный номер =)
    private void UIDL_uniq_number() {
        String exedir = new File(".").getAbsolutePath();
        String dst = exedir.substring(0, exedir.length() - 2);

        File path = new File(dst + File.separator + path_to_mailbox + File.separator);
        File[] list = path.listFiles();

        // количество байт всех сообщений
        int size = 0;
        for (File file : list) {
            size += file.length();
        }
        out.print("+OK " + list.length + " messages (" + size + " octets)" + CRLF);
        System.out.println("+OK " + list.length + " messages (" + size + " octets)" + CRLF);
        for (int i = 1; i <= list.length; i++) {
            out.println(i + " " + i);
            System.out.println(i + " " + i);
        }
        out.println(EOM);
        System.out.println(EOM);

    }

    private void RETR_transmit_message(String client_inp) {
        File path = new File(path_to_mailbox + File.separator);
        File[] list = path.listFiles();

        // парсим строку, выбирая номер сообщения для скачивания
        StringTokenizer st = new StringTokenizer(client_inp, " ");
        st.nextToken();

        try{
            int number_of_message = Integer.parseInt(st.nextToken());

            if(number_of_message-1 >= list.length){
                out.println("-ERR no such message");
            } else {
                out.println("+OK message follows\r\n");
                // открываем файл сообщения
                try {
                    BufferedReader buff = new BufferedReader(new FileReader(path_to_mailbox + list[number_of_message - 1].getName()));

                    while (true) {
                        try {
                            String line = buff.readLine();
                            if (line == null) break;
                            if (!line.equals("")) out.println(line);
                            else out.println(CRLF);
                        } catch (IOException ex) {
                            System.out.println("Can't read chosen message file");
                            System.exit(-4);
                        }
                    }

                } catch (FileNotFoundException ex) {
                    System.out.println("Can't find chosen message file");
                    System.exit(-3);
                }
                out.println(EOM);
            }

        } catch (NumberFormatException e) {
            out.println("-ERR no such message");
        }
    }

    private void QUIT_end_connection() {
        File path = new File(path_to_mailbox + File.separator);
        File[] file_list = path.listFiles();

        for (int i : list_to_del) {
            file_list[i - 1].delete();
        }

        list_to_del.clear();

        out.println("+OK");
        path_to_mailbox = "database" + File.separator + "inbox";
        authorization = true;
        transaction = false;
    }

    private void DELE_mark_to_del(String client_inp) {
        out.println("+OK message deleted");
        StringTokenizer st = new StringTokenizer(client_inp, " ");
        st.nextToken();
        int number_of_message = Integer.parseInt(st.nextToken());

        list_to_del.add(number_of_message);
        System.out.println(list_to_del);
    }

    private void TOP_show_mail_top(String client_inp) {
        BufferedReader buff = null;

        File path = new File(path_to_mailbox + File.separator);
        File[] list = path.listFiles();

        // парсим строку, выбирая номер сообщения
        StringTokenizer st = new StringTokenizer(client_inp, " ");
        st.nextToken();
        int number_of_message = Integer.parseInt(st.nextToken());

        // открываем файл сообщения
        try {
            buff = new BufferedReader(new FileReader(path_to_mailbox + list[number_of_message - 1].getName()));

        } catch (FileNotFoundException ex) {
            System.out.println("Can't find chosen message file");
            System.exit(-3);
        }


        while (true) {
            try {
                String line = buff.readLine();
                if (line == null) break;
                if (!line.equals("")) out.println(line + CRLF);
                else break;
            } catch (IOException ex) {
                System.out.println("Can't read chosen message file");
                System.exit(-4);
            }
        }
        out.println(EOM);

    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new PrintWriter(clientSocket.getOutputStream(), true);

            try {
                // приветственное сообщение
                out.println("+OK POP3 server ready");

                while (true) {
                    String input = in.readLine();
                    System.out.println("text from client with port " + clientSocket.getPort() + ":::  " + input);

                    if(input == null){
                        System.out.println("Close client with port " + clientSocket.getPort());
                        break;
                    }

                    try {
                        StringTokenizer st = new StringTokenizer(input, " ");
                        String cmd = st.nextToken();
                        if (authorization) {
                            if (cmd.equals(CAPA)) CAPA_available_cmds();
                            else if (cmd.equals(USER)) USER_parse_login(input);
                            else if (cmd.equals(PASS)) PASS_password_control(input);
                            else if (cmd.equals(QUIT)) {
                                QUIT_end_connection();
                                System.out.println("Close client with port " + clientSocket.getPort());
                                break;
                            } else out.println("-ERR unknown command");
                        } else if (transaction) {
                            if (cmd.equals(STAT)) STAT_discovery_dir();
                            else if (cmd.equals(RETR)) RETR_transmit_message(input);
                            else if (cmd.equals(LIST)) LIST_size_list();
                            else if (cmd.equals(UIDL)) UIDL_uniq_number();
                            else if (cmd.equals(DELE)) DELE_mark_to_del(input);
                            else if (cmd.equals(NOOP)) out.println("+OK");
                            else if (cmd.equals(TOP)) TOP_show_mail_top(input);
                            else if (cmd.equals(QUIT)) {
                                QUIT_end_connection();
                                System.out.println("Close client with port " + clientSocket.getPort());
                                break;
                            } else out.println("-ERR unknown command");
                        }
                    } catch (Exception e) {
                        out.println("-ERR unknown command");
                    }
                }

                out.close();
                in.close();
                clientSocket.close();
            } catch (IOException ex) {
                System.out.println("Problem with streams");
                System.exit(-6);
            }

        } catch (IOException ex) {
            System.out.println("Can't read socket");
            return;
        }
    }
}



