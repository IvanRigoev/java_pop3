package pop3server;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;


public class ServerPOP3 {
    private static ServerSocket server = null;

    public static void exit() throws IOException {
        System.out.println("Exit subprogram is running");
        server.close();
    }


    public static void main(String[] args) throws IOException {

        String exedir = new File(".").getAbsolutePath();
        String dst = exedir.substring(0, exedir.length() - 2);

        System.out.println(dst);

        // создаем серверный сокет
        try {
//            server = new ServerSocket(110, 1005);
            server = new ServerSocket(110);
        } catch (IOException ex) {
            System.out.println("Couldn't listen to port 110");
            System.exit(-1);
        }

        // Создаем потоки принятия клиентов
        ServerThread serverThread = new ServerThread(server);
        serverThread.start();

        BufferedReader inu = new BufferedReader(new InputStreamReader(System.in));

        String fuser;

        while (true) {
            fuser = inu.readLine();
            if (fuser.equalsIgnoreCase("exit")) {
                exit();
                break;
            }

        }
    }
}
